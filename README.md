# useful utilities

## kubernetes
### kube-linter

allows linting of Kubernetes YAMLs and Helm charts

usage:
```
kube-linter lint 'filename'
```

usage via docker, custom Dockerfile included in kube-linter_docker folder


```
docker build -t username/linter .
docker run username/linter [filename]
```

### export a running pod/service/rs etc. to yaml file


```
kubectl get pod [pod_name] -o yaml > [pod_name].yaml 
```

### this one's kinda similiar - generate a fresh POD yaml file without creating it on the cluster

```
kubectl run nginx --image=nginx --dry-run=client -o yaml >> nginx-pod.yaml
```

### override docker image entrypoint

in pod yaml file, under "containers:" add

``` 
command: [''overridden_example'']

```


## aws

### make aws-vault use multifactor authentication

add to .aws/config 

```

[profile skmtrn]

mfa_serial = [put mfa id from IAM here]
```

## ansible


### install ansible-galaxy role in a given directory

```
ansible-galaxy install [role_name] -p [path]
```


### make a cron job running every 5/10/15 minutes etc.



using cron module
```
cron:
  name: example job
  job: path to script or command

  minute: */[5; 10; 15]


``` 


## terraform

### run terraform locally with docker-compose

docker-compose-terraform.yml included, make sure you're running it in deploy dir

usage:

```
docker-compose run --rm terraform [command; init, test, apply, destroy, etc.]
```


### use latest AMI from amazon


in a desired_resource.tf file:

```
data "aws_ami" "amazon_linux" {

  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]


  }
  owners = ["amazon"]

}
```

then


```

resource "aws_instance" "bastion" {

  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

}



```



## gitlab CI yml tweaks

### proceed with a stage when merge request target or commit branch has "master" or "production" in the name

```
- if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(master|production)$/ || $CI_COMMIT_BRANCH =~ /^(master|production)$/'

```

### push Docker image into ECR twice: tagged with a commit checksum and a 'latest' tag

after setting AWS ECR_REPO environmental variable in GitLab CI settings:

```

- $(aws ecr get-login --no-include-email --region eu-central-1)
- docker push $ECR_REPO:$CI_COMMIT_SHORT_SHA
- docker tag $ECR_REPO:$CI_COMMIT_SHORT_SHA $ECR_REPO:latest
- docker push $ECR_REPO:latest

```


## various

### list processes using given directory with lsof


usage:
```
lsof -n | grep 'dir_name'
```
