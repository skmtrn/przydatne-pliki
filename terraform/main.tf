terraform {
  required_version = ">= 0.12"
  backend "s3" {

    bucket         = "wiadro-sylwka"
    key            = "testowa-apka.tfstate"
    region         = "eu-west-3"
    encrypt        = true
    dynamodb_table = "testowa-apka-tf-state-lock"
  }
}

provider "aws" {
  region  = "eu-west-3"
  version = "~> 2.54.0"
}


locals {

  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {

    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"

  }
}


